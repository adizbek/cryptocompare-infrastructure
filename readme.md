## Infrastructure config of cryptocompare app
App repository is located here: https://gitlab.com/adizbek/cryptocompare

### Running inside docker (swarm) for production usage
To deploy application run `docker stack deploy -c stack.yml cryptocompare`

To stop stack run `docker stack rm cryptocompare`
